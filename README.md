# Auto Vote Rating - Chrome Extension
<details>
<summary>List of sites that the extension supports</summary>
<a href="http://topcraft.ru/">topcraft.ru</a>  
<a href="https://mctop.su/">mctop.su</a>  
<a href="http://mcrate.su/">mcrate.su</a>  
<a href="http://minecraftrating.ru/">minecraftrating.ru</a>  
<a href="http://monitoringminecraft.ru/">monitoringminecraft.ru</a>  
<a href="https://ionmc.top/">ionmc.top</a>  
<a href="https://minecraftservers.org/">minecraftservers.org</a>  
<a href="https://serveur-prive.net/minecraft">serveur-prive.net</a>  
<a href="https://www.planetminecraft.com/">planetminecraft.com</a>  
<a href="https://topg.org/minecraft">topg.org</a>  
<a href="https://minecraft-mp.com/">minecraft-mp.com</a>  
<a href="http://minecraft-server-list.com/">minecraft-server-list.com</a>  
<a href="https://www.serverpact.com/">serverpact.com</a>  
<a href="https://www.minecraftiplist.com/">minecraftiplist.com</a>  
<a href="https://topminecraftservers.org/">topminecraftservers.org</a>  
<a href="http://minecraftservers.biz/">minecraftservers.biz</a>  
<a href="https://hotmc.ru/">hotmc.ru</a>  
<a href="https://minecraft-server.net/">minecraft-server.net</a>  
<a href="https://top-games.net/">top-games.net или top-serveurs.net</a>  
<a href="https://tmonitoring.com/">tmonitoring.com</a>  
<a href="https://top.gg/">top.gg</a>  
<a href="https://discordbotlist.com/">discordbotlist.com</a>  
<a href="https://discords.com/">discords.com</a>  
<a href="https://mmotop.ru/">mmotop.ru</a>  
<a href="https://mc-servers.com/">mc-servers.com</a>  
<a href="https://minecraftlist.org/">minecraftlist.org</a>  
<a href="https://www.minecraft-index.com/">minecraft-index.com</a>  
<a href="https://serverlist101.com/">serverlist101.com</a>  
<a href="https://mcserver-list.eu/">mcserver-list.eu</a>  
<a href="https://craftlist.org/">craftlist.org</a>  
<a href="https://czech-craft.eu/">czech-craft.eu</a>  
<a href="https://minecraft.buzz/">minecraft.buzz</a>  
<a href="https://minecraftservery.eu/">minecraftservery.eu</a>  
<a href="https://www.rpg-paradize.com/">rpg-paradize.com</a>  
<a href="https://www.minecraft-serverlist.net/">minecraft-serverlist.net</a>  
<a href="https://minecraft-server.eu/">minecraft-server.eu</a>  
<a href="https://www.minecraftkrant.nl/">minecraftkrant.nl</a>  
<a href="https://www.trackyserver.com/">trackyserver.com</a>  
<a href="https://mc-lists.org/">mc-lists.org</a>  
<a href="https://topmcservers.com/">topmcservers.com</a>  
<a href="https://bestservers.com/">bestservers.com</a>  
<a href="https://craft-list.net/">craft-list.net</a>  
<a href="https://www.minecraft-servers-list.org/">minecraft-servers-list.org</a>  
<a href="https://www.serverliste.net/">serverliste.net</a>  
<a href="https://gtop100.com/">gtop100.com</a>  
<a href="https://wargm.ru/s">wargm.ru</a>  
<a href="https://minestatus.net/">minestatus.net</a>  
<a href="https://misterlauncher.org/">misterlauncher.org</a>  
<a href="https://minecraft-servers.de/">minecraft-servers.de</a>  
<a href="https://discord.boats/">discord.boats</a>  
<a href="https://serverlist.games/">serverlist.games</a>  
<a href="https://best-minecraft-servers.co/">best-minecraft-servers.co</a>  
<a href="https://minecraftservers100.com/">minecraftservers100.com</a>  
<a href="https://mc-serverlist.cz/">mc-serverlist.cz</a>  
<a href="https://mineservers.com/">mineservers.com</a>  
<a href="https://atlauncher.com/">atlauncher.com</a>  
<a href="https://servers-minecraft.net/">servers-minecraft.net</a>  
<a href="https://www.minecraft-list.cz/">minecraft-list.cz</a>  
<a href="https://www.liste-serveurs-minecraft.org/">liste-serveurs-minecraft.org</a>  
<a href="https://mcservidores.com/">mcservidores.com</a>  
<a href="https://www.xtremetop100.com/">xtremetop100.com</a>  
<a href="https://minecraft-server.sk/">minecraft-server.sk</a>  
<a href="https://www.serveursminecraft.org/">serveursminecraft.org</a>  
<a href="https://serveurs-mc.net/">serveurs-mc.net</a>  
<a href="https://serveur-minecraft.com/">serveur-minecraft.com</a>  
<a href="https://serveur-minecraft-vote.fr/">serveur-minecraft-vote.fr</a>  
<a href="https://minebrowse.com/">minebrowse.com</a>  
<a href="https://mc-server-list.com/">mc-server-list.com</a>  
<a href="https://serverlocator.com/">serverlocator.com</a>  
<a href="https://top-mmogames.ru/">top-mmogames.ru</a>  
<a href="https://mmorpg.top/">mmorpg.top</a>  
<a href="https://mmovote.ru/">mmovote.ru</a>  
<a href="https://mc-monitoring.info/">mc-monitoring.info</a>  
</details>

### Links to the extension where you can install it:
[Chrome Web Store](https://chrome.google.com/webstore/detail/auto-vote-minecraft-ratin/mdfmiljoheedihbcfiifopgmlcincadd)   
[Firefox Add-ons](https://addons.mozilla.org/ru/firefox/addon/auto-vote-rating/)   
[Opera Addons](https://addons.opera.com/ru/extensions/details/auto-vote-minecraft-rating/)   
[Microsoft Edge Add-ons](https://microsoftedge.microsoft.com/addons/detail/auto-vote-rating/ecoifpgiojfhmihcfomafdcmkphafpba)
## Install the extension from zip archive
Here is a short guide how to install an unpacked extension on Google Chrome or Chromium-based browsers
1. [Download](https://gitlab.com/Serega007/auto-vote-rating/-/archive/dev/auto-vote-rating-dev.zip) this repository
2. Unzip the archive to any convenient location
3. Open the "Extensions" page `chrome://extensions/` in the browser and turn on the "Developer mode"

![](https://i.imgur.com/iQ4DXVu.png)

4. Click on the "Load unpacked" button and select the directory where you unpacked the archive.